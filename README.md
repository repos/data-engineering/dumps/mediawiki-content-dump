# mediawiki-content-dump
PySpark project with data pipelines to generate a table that contains all the content of all the wikis over all of wiki time.

This is part of the effort to build Dumps 2.0. See https://phabricator.wikimedia.org/T330296 for details.

## Running Tests

Make sure you have conda in your path. Then set up an environment:
```bash
# create conda environment
conda env create -f conda-environment.yaml
conda activate mediawiki_content_dump
# install test dependencies
conda install -c conda-forge -y pytest=7.4.0 pytest-cov=4.1.0
# execute test suite
python -m pytest --cov mediawiki_content_dump --cov-report term --cov-report xml
```
