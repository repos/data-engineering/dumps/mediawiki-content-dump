CREATE TABLE wmf_content.inconsistent_rows_of_mediawiki_content_history_v1 (
    wiki_id                        STRING              COMMENT 'The wiki ID, which is usually the same as the MediaWiki database name. E.g. enwiki, metawiki, etc.',
    page_id                        BIGINT              COMMENT 'The (database) page ID of the page.',
    revision_id                    BIGINT              COMMENT 'The (database) revision ID.',
    revision_dt                    TIMESTAMP           COMMENT 'The (database) time this revision was created. This is rev_timestamp in the MediaWiki database.',
    reasons                        ARRAY<STRING>       COMMENT 'The set of reasons detected that make us think we need to reconcile this revision.',
    computation_dt                 TIMESTAMP           COMMENT 'The logical time at which this inconsistency was calculated. Useful to see trends over time, and also to be able to delete data efficiently.',
    computation_class              STRING              COMMENT 'One of "last-24h" or "all-of-wiki-time". This segregates between runs that cover one day of inconsistencies as of computation_dt, versus runs that retroactively check all revisions as of computation_dt.',
    reconcile_emit_dt              TIMESTAMP           COMMENT 'The time at which this inconsistency was emitted for eventual reconcile. If NULL, it has not been submitted yet.'
)
USING ICEBERG
PARTITIONED BY (wiki_id, computation_class)
TBLPROPERTIES (
    'format-version' = '2',                          -- allow merge-on-read if needed
    'write.format.default' = 'parquet',              -- parquet is currently the only format with min/max stats
    'write.target-file-size-bytes' = '134217728',    -- cap files at 128MB files
    'commit.retry.num-retries' = '10'                -- bump retries from default of 4 due to many concurrent INSERTs
)
COMMENT 'We make checks between wmf_content.mediawiki_content_history_v1 and the Analytics replicas to detect inconsistent rows. If we do detect any, we add them here, to be reconciled, alerted, and analyzed.'
LOCATION '/wmf/data/wmf_content/inconsistent_rows_of_mediawiki_content_history_v1'


-- WRITE ORDERED BY is an Iceberg SQL extension, and thus not available in the CREATE syntax as of Spark 3.3
-- THIS ALTER TABLE should be run immediately after the CREATE
ALTER TABLE wmf_content.inconsistent_rows_of_mediawiki_content_history_v1 WRITE ORDERED BY wiki_id, computation_class, computation_dt, revision_dt
