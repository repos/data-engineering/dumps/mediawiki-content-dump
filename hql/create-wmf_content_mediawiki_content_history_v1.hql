CREATE TABLE wmf_content.mediawiki_content_history_v1 (
    page_id                     BIGINT    COMMENT 'The (database) page ID of the page.',
    page_namespace_id           INT       COMMENT 'The id of the namespace this page belongs to.',
    page_title                  STRING    COMMENT 'The normalized title of the page. If page_namespace_id = 0, then this is the non namespaced title. If page_namespace_id != 0, then the title is prepended with the localized namespace. Examples for "enwiki": "Main_Page" and "Talk:Main_Page".',
    page_redirect_target        STRING    COMMENT 'title of the redirected-to page, if any. Same rules as page_title.',
    user_id                     BIGINT    COMMENT 'id of the user that made the revision; null if anonymous, zero if old system user, and -1 when deleted or malformed XML was imported',
    user_text                   STRING    COMMENT 'text of the user that made the revision (either username or IP)',
    user_is_visible             BOOLEAN   COMMENT 'Whether the user that made the revision is visible. If this is false, then the user should be redacted when shown publicly. See RevisionRecord->DELETED_USER.',
    revision_id                 BIGINT    COMMENT 'The (database) revision ID.',
    revision_parent_id          BIGINT    COMMENT 'The (database) revision ID. of the parent revision',
    revision_dt                 TIMESTAMP COMMENT 'The (database) time this revision was created. This is rev_timestamp in the MediaWiki database.',
    revision_is_minor_edit      BOOLEAN   COMMENT 'True if the editor marked this revision as a minor edit.',
    revision_comment            STRING    COMMENT 'The comment left by the user when this revision was made.',
    revision_comment_is_visible BOOLEAN   COMMENT 'Whether the comment of the revision is visible. If this is false, then the comment should be redacted when shown publicly. See RevisionRecord->DELETED_COMMENT.',
    revision_sha1               STRING    COMMENT 'Nested SHA1 hash of hashes of all content slots. See https://www.mediawiki.org/wiki/Manual:Revision_table#rev_sha1',
    revision_size               BIGINT    COMMENT 'the sum of the content_size of all content slots',
    revision_content_slots      MAP<
                                    STRING,
                                    STRUCT<content_body:   STRING,
                                           content_format: STRING,
                                           content_model:  STRING,
                                           content_sha1:   STRING,
                                           content_size:   BIGINT
                                    >
                                >         COMMENT 'a MAP containing all the content slots associated to this revision. Typically just the "main" slot, but also "mediainfo" for commonswiki.',
    revision_content_is_visible BOOLEAN   COMMENT 'Whether revision_content_slots is visible. If this is false, then any content should be redacted when shown publicly. See RevisionRecord->DELETED_TEXT.',
    wiki_id                     STRING    COMMENT 'The wiki ID, which is usually the same as the MediaWiki database name. E.g. enwiki, metawiki, etc.',
    row_content_update_dt       TIMESTAMP COMMENT 'Control column. Marks the timestamp of the last content event or backfill that updated this row',
    row_visibility_update_dt    TIMESTAMP COMMENT 'Control column. Marks the timestamp of the last visibility event or backfill that updated this row',
    row_move_update_dt          TIMESTAMP COMMENT 'Control column. Marks the timestamp of the last move event or backfill that updated this row'
)
USING ICEBERG
PARTITIONED BY (wiki_id)                             -- wiki_id partitioning is familiar to users
TBLPROPERTIES (
    'format-version' = '2',                          -- allow merge-on-read
    'write.format.default' = 'parquet',              -- parquet is currently the only format with min/max stats
    'write.target-file-size-bytes' = '134217728',    -- cap files at 128MB files so executors with 1 core, 16GB RAM can read the entire table
    'write.metadata.previous-versions-max' = '10',
    'write.metadata.delete-after-commit.enabled' = 'true'
)
COMMENT 'Contains all of the revisions for all pages for all wikis. Updated on a daily basis.'
LOCATION '/wmf/data/wmf_content/mediawiki_content_history_v1'

-- WRITE ORDERED BY is an Iceberg SQL extension, and thus not available in the CREATE syntax as of Spark 3.3
-- THIS ALTER TABLE should be run immediately after the CREATE
ALTER TABLE wmf_content.mediawiki_content_history_v1 WRITE ORDERED BY wiki_id, page_id, revision_dt
