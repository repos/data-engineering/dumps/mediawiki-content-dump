import argparse
import sys
import os
from functools import partial
from datetime import datetime

# Pydeequ is dependent on the Spark version so set before importing
SPARK_VERSION = "3.3"
os.environ.setdefault('SPARK_VERSION', SPARK_VERSION)

import pydeequ
from pydeequ.analyzers import AnalyzerContext, AnalysisRunner, Completeness, Size, Uniqueness, Maximum
from pydeequ.repository import InMemoryMetricsRepository, MetricsRepository, ResultKey
from pyspark.sql import DataFrame, Row, SparkSession
from refinery_python.dataset import HivePartition
from refinery_python.dq import DeequAnalyzersToDataQualityMetrics


def parse_args(args) -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Computes last-24h data quality metrics on a window of rows.'
    )
    parser.add_argument('--wiki_ids', help='comma-separated list of wiki_ids to compute metrics on.', required=True)
    parser.add_argument('--content_table', help='data lake table to compute metrics on.', required=True)
    parser.add_argument('--inconsistent_rows_table', help='data lake table to compute metrics on.', required=True)
    parser.add_argument('--metrics_table', help='data lake table to write metrics to.', required=True)
    parser.add_argument('--min_timestamp', help='the inclusive minimum revision_dt to compute metrics on.', required=True)
    parser.add_argument('--max_timestamp', help='the exclusive maximum revision_dt to compute metrics on.', required=True)
    parser.add_argument('--run_id', help='the run id of the dag, attached to generated metrics.', required=True)

    args = parser.parse_args(args)
    args.wiki_ids = [wiki_id.strip() for wiki_id in args.wiki_ids.split(',')]
    return args


def compute_content_table_deequ_metrics(
    spark: SparkSession,
    repository: MetricsRepository,
    result_key: ResultKey,
    wiki_id: str,
    content_table: str,
    revision_dt_start: str,
    revision_dt_end: str
) -> DataFrame:
    df = (spark.table(f"{content_table}")
        .where(f"revision_dt >= CAST('{revision_dt_start}' AS TIMESTAMP)")
        .where(f"revision_dt < CAST('{revision_dt_end}' AS TIMESTAMP)")
        .where(f"wiki_id = '{wiki_id}'"))

    # Analyzers can only be run on columns, so flatten revision_content_slots to support T382953
    flattened_df = (df
        .withColumn("revision_content_slots.main.content_body",
                    df["revision_content_slots"]["main"]["content_body"])
        .withColumn("revision_content_slots.main.content_format",
                    df["revision_content_slots"]["main"]["content_format"])
    )

    analysisResult = (AnalysisRunner(spark).onData(flattened_df)
        .addAnalyzer(Size())
        .addAnalyzer(Completeness("page_id"))
        .addAnalyzer(Completeness("revision_id"))
        .addAnalyzer(Completeness("revision_content_slots.main.content_body"))
        .addAnalyzer(Completeness("revision_content_slots.main.content_format"))
        .addAnalyzer(Uniqueness(["page_id", "revision_id"]))
        .useRepository(repository)
        .saveOrAppendResult(result_key)
        .run())
    
    return AnalyzerContext.successMetricsAsDataFrame(spark, analysisResult)


def compute_inconsistent_rows_table_deequ_metrics(
    spark: SparkSession,
    repository: MetricsRepository,
    result_key: ResultKey,
    wiki_id: str,
    inconsistent_rows_table: str,
    revision_timestamp_start: str,
    revision_timestamp_end: str
) -> DataFrame:
    df = (spark.table(f"{inconsistent_rows_table}")
        .where(f"computation_dt = CAST('{revision_timestamp_end}' AS TIMESTAMP)")
        .where(f"computation_class = 'last-24h'")
        .where(f"wiki_id = '{wiki_id}'"))

    analysisResult = (AnalysisRunner(spark).onData(df)
        .addAnalyzer(Size())
        .addAnalyzer(Completeness("page_id"))
        .addAnalyzer(Completeness("revision_id"))
        .addAnalyzer(Uniqueness(["page_id", "revision_id"]))
        .useRepository(repository)
        .saveOrAppendResult(result_key)
        .run())

    return AnalyzerContext.successMetricsAsDataFrame(spark, analysisResult)


def insert_arbitrary_custom_metric_value(
    spark: SparkSession,
    repository: MetricsRepository,
    result_key: ResultKey,
    instance_name: str,
    value: float
) -> DataFrame:
    # It's not possible to directly insert a metric without an analyzer
    # so create a DataFrame with a single record
    # and then use the Maximum analyzer to put it in the repository
    df = spark.createDataFrame([Row(**{f'{instance_name}': value})])
    analysisResult = (AnalysisRunner(spark).onData(df)
        .addAnalyzer(Maximum(instance_name))
        .useRepository(repository)
        .saveOrAppendResult(result_key)
        .run())
    df = AnalyzerContext.successMetricsAsDataFrame(spark, analysisResult)
    return df


def compute_inconsistent_rows_ratio(
    spark: SparkSession,
    cross_table_repository: MetricsRepository,
    content_repository: MetricsRepository,
    inconsistent_repository: MetricsRepository,
    result_key: ResultKey
) -> MetricsRepository:
    content_size = (content_repository.repository
                    .loadByKey(result_key.resultKey).get()
                    .metric(Size()._set_jvm(spark._jvm)._analyzer_jvm).get()
                    .value().get())

    inconsistent_size = (inconsistent_repository.repository
                         .loadByKey(result_key.resultKey).get()
                         .metric(Size()._set_jvm(spark._jvm)._analyzer_jvm).get()
                         .value().get())
    
    return insert_arbitrary_custom_metric_value(
        spark,
        cross_table_repository,
        result_key,
        "inconsistent_rows_ratio",
        # If content_size is 0 and inconsistent_size isn't then we have a problem
        inconsistent_size / content_size if content_size else inconsistent_size
    )

def create_hive_partition(
    spark: SparkSession,
    db: str,
    table: str,
    wiki_id: str,
    year: str,
    month: str,
    day: str
) -> HivePartition:
    # Fake hive partition.
    # A hive partition is required to generate the metrics, but since the tables
    # are Iceberg tables, this has no real meaning
    # TODO: Implement IcebergPartition
    return HivePartition(
        spark,
        db,
        table,
        None,
        {
            "wiki_id": wiki_id,
            "year": year,
            "month": month,
            "day": day,
            "hour": "0",
        },
    )


def save_metrics(spark: SparkSession, metrics_table: str, repository: MetricsRepository, partition: HivePartition, run_id: str):
    metrics = DeequAnalyzersToDataQualityMetrics(spark, repository, partition, run_id)
    metrics_writer = metrics.write().iceberg().output(metrics_table)
    metrics_writer.save()


def main() -> None:  # pragma: no cover
    args = parse_args(sys.argv[1:])
    spark = SparkSession.builder.getOrCreate()
        
    content_db, content_table = args.content_table.split('.')
    inconsistent_rows_db, inconsistent_rows_table = args.inconsistent_rows_table.split('.')
    min_timestamp = datetime.strptime(args.min_timestamp, '%Y-%m-%dT%H:%M:%S')
    
    # Delete old metrics incase of rerun
    spark.sql(f"""
              DELETE FROM {args.metrics_table}
              WHERE partition_ts = CAST('{args.min_timestamp}' AS TIMESTAMP)
                AND (source_table = '`{content_db}`.`{content_table}`'
                  OR source_table = '`{inconsistent_rows_db}`.`{inconsistent_rows_table}`'
                  OR source_table = '`{content_db}`.`{inconsistent_rows_table}_{content_table}`'
                )
              """)
    
    for wiki_id in args.wiki_ids:
        print("Processing " + wiki_id)
        create_partition = partial(
            create_hive_partition,
            spark=spark,
            wiki_id=wiki_id,
            year=str(min_timestamp.year),
            month=str(min_timestamp.month),
            day=str(min_timestamp.day)
        )

        # We will use the same result key for every metric computation    
        result_key = ResultKey(
            spark, ResultKey.current_milli_time(), {"wiki_id": f"{wiki_id}", "project": "mediawiki_content_history"}
        )

        content_repository = InMemoryMetricsRepository(spark)
        content_metrics = compute_content_table_deequ_metrics(
            spark,
            content_repository,
            result_key,
            wiki_id,
            args.content_table,
            args.min_timestamp,
            args.max_timestamp
        )
        content_partition = create_partition(db=content_db, table=content_table)
        save_metrics(spark, args.metrics_table, content_repository, content_partition, args.run_id)
    
        inconsistent_repository = InMemoryMetricsRepository(spark)
        inconsistent_metrics = compute_inconsistent_rows_table_deequ_metrics(
            spark,
            inconsistent_repository,
            result_key,
            wiki_id,
            args.inconsistent_rows_table,
            args.min_timestamp,
            args.max_timestamp
        )
        inconsistent_partition = create_partition(db=inconsistent_rows_db, table=inconsistent_rows_table)
        save_metrics(spark, args.metrics_table, inconsistent_repository, inconsistent_partition, args.run_id)

        cross_table_repository = InMemoryMetricsRepository(spark)
        cross_table_partition = create_partition(db=content_db, table=f'{inconsistent_rows_table}/{content_table}')
        compute_inconsistent_rows_ratio(
            spark,
            cross_table_repository,
            content_repository,
            inconsistent_repository,
            result_key
        )
        save_metrics(spark, args.metrics_table, cross_table_repository, cross_table_partition, args.run_id)

    # Shut down the Spark session to prevent any hanging processes.
    spark.sparkContext._gateway.shutdown_callback_server()
    spark.stop()