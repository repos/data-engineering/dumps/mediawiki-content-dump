import re
import argparse
import sys

from datetime import datetime
from pyspark.sql import SparkSession

from .replicas_util import resolve_mariadb_host_port_for_wikidb, resolve_pw_for_analytics_mariadb_replicas


def parse_args(args) -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Runs consistency checks between a source of truth mariadb replica and a target data lake table.'
    )
    parser.add_argument('--wiki_id', help='wiki_id to do quality checks against.', required=True)
    parser.add_argument('--target_table', help='data lake table to do consistency checks against.', required=True)
    parser.add_argument('--results_table', help='data lake table to write results to.', required=True)
    parser.add_argument('--min_timestamp', help='a timestamp that bounds the lower time limit to check consistency for.', required=True)
    parser.add_argument('--max_timestamp', help='a timestamp that bounds the upper time limit to check consistency for.', required=True)
    parser.add_argument('--computation_dt', help='The logical time for which this consistency check is being run. Will be written in the results table.', required=True)
    parser.add_argument('--computation_class', choices=['last-24h', 'all-of-wiki-time'],
                        help='This segregates between runs that cover one day of inconsistencies as of computation_dt, '
                             'versus runs that retroactively check all revisions as of computation_dt.',
                        required=True)
    parser.add_argument('--mariadb_username', help='The Analytics Replica MariaDB username.', required=True)
    parser.add_argument('--mariadb_password_file', help='The HDFS file path for the password to connect to the Analytics Replica MariaDB instance.', required=True)

    return parser.parse_args(args)


"""
Given a column to filter by and upper/lower bounds,
returns a dataframe of a custom JDBC query.  If partition options are set,
guesses at an optimal number of partitions, up to the 64.

Example use (NOTE: timestamps in ISO format are fine, this will convert to MW):

password = 'REDACTED' # in ipython use the history

import re
import math
from datetime import datetime

wiki_id = 'etwiki'
table = 'revision'
bound_column = 'rev_timestamp'
lower_bound = '2023-12-01T00:00:00'
upper_bound = '2024-02-01T00:00:00'
host = 'dbstore1007.eqiad.wmnet'
port = '3313'
user = 'research'

df = df_from_mariadb_replica_adaptive(
    spark,
    '''
    SELECT rev_id                          AS revision_id,
           rev_page                        AS page_id,
           rev_timestamp,
           CAST(rev_timestamp as DATETIME) AS revision_dt,
           rev_deleted & 1 = 0             AS revision_content_is_visible,
           rev_deleted & 2 = 0             AS user_is_visible,
           rev_deleted & 4 = 0             AS revision_comment_is_visible,
           rev_len                         AS revision_size,
           rev_sha1                        AS revision_sha1
    ''',
    None,
    table,
    bound_column,
    lower_bound,
    upper_bound,
    wiki_id,
    host,
    port,
    user,
    password,
    # the following are left out because they're defaults:
    # partition_by_bound_column=True,
    # force_index_name="rev_timestamp",
)

df.write.saveAsTable(f'milimetric.test_read_{wiki_id}_{table}', mode='overwrite')
"""
def df_from_mariadb_replica_adaptive(
        spark,
        # NOTE: include the partition column in the select clause
        select_clause,
        # NOTE: will automatically add a filter for your lower and upper bounds
        where_clause,
        table,
        # can be timestamps or numeric columns, but not strings for now
        bound_column,
        # timestamps can be in ISO or MW format, both work
        lower_bound,
        upper_bound,
        wiki_id,
        mariadb_args,
        # set this to False to only filter by the bound column and not partition by it
        # if true, will automatically optimize the partitions adaptively, based on the size of the table
        partition_by_bound_column=True,
        # optional - the name of the MariaDB index on the partition column
        # defaults to bound_column
        force_index_name=None,
):
    # normalize parameters
    if not force_index_name:
        force_index_name = bound_column
    lower_bound = re.sub(r"[^0-9]*", "", lower_bound)
    upper_bound = re.sub(r"[^0-9]*", "", upper_bound)

    # general options
    opts = {
        "driver" : "com.mysql.cj.jdbc.Driver",
        "url": f"jdbc:mysql://{mariadb_args['host']}:{mariadb_args['port']}/{wiki_id}",
        "user": mariadb_args['username'],
        "password": mariadb_args['pw']
    }
    force_index = f"FORCE INDEX ({force_index_name})"

    def update_query_and_log(query):
        opts.update({"dbtable": f"({query}) t"})
        print(f'INFO: Running query on {opts["url"]}: {query}')

    # get table min/max bound_column values for whole table (query also tests FORCE INDEX)
    update_query_and_log(f"""
         select min({bound_column}), max({bound_column})
           from {table} {force_index}
        """)
    boundary_df = spark.read.format("mediawiki-jdbc").options(**opts).load()
    boundary = boundary_df.take(1)[0]
    table_lower_bound, table_upper_bound = boundary[0], boundary[1]

    # estimate the count of the slice we're querying by assuming an even distribution
    bound_column_type = boundary_df.schema.fields[0].dataType.typeName()
    match bound_column_type:
        case "binary":
            if not len(lower_bound) == 14:
                raise TypeError("only mw timestamp binary types are supported, like rev_timestamp")
            mw_iso = "%Y%m%d%H%M%S"
            start_timestamp = datetime.strptime(lower_bound, mw_iso)
            end_timestamp = datetime.strptime(upper_bound, mw_iso)
            try:
                first_timestamp = datetime.strptime(table_lower_bound.decode('ascii'), mw_iso)
            except ValueError:
                # workaround https://phabricator.wikimedia.org/T378603
                # some wikis have rows with blank rev_timestamps ¯\_(ツ)_/¯
                print("WARN: Found blank rev_timestamp. Defaulting first_timestamp to '2000-01-01T00:00:00'. T378603.")
                first_timestamp = datetime.strptime('20000101000000', mw_iso)

            last_timestamp = datetime.strptime(table_upper_bound.decode('ascii'), mw_iso)

            estimate_ratio = max((end_timestamp - start_timestamp).days, 1) / max((last_timestamp - first_timestamp).days, 1)
            # timestamps must be in quotes to use the index properly
            bound_column_filter = f"{bound_column} between '{lower_bound}' and '{upper_bound}'"

        case "long" | "decimal":
            estimate_ratio = max((int(upper_bound) - int(lower_bound)), 1) / max((table_upper_bound - table_lower_bound), 1)
            # numbers not in quotes
            bound_column_filter = f"{bound_column} between {lower_bound} and {upper_bound}"

    if partition_by_bound_column:
        # check how big the whole table is, fast query against info schema
        # (couldn't find min/max values per column in information_schema tables)
        update_query_and_log(f"""
             select cardinality
               from information_schema.statistics
              where TABLE_NAME = '{table}'
                and TABLE_SCHEMA = '{wiki_id}'
                and INDEX_NAME = 'PRIMARY'
            """)
        table_count = spark.read.format("mediawiki-jdbc").options(**opts).load().take(1)[0][0]

        # We want this function to grow linearly as follows:
        #     500,000: 1
        #  50,000,000: 20
        # 500,000,000: 200
        # with boundaries 1, 256
        rows_estimate = table_count * estimate_ratio
        num_partitions = max(min(round(rows_estimate / 2500000), 256), 1)

        # This num_partitions is fed to the Spark jdbc datasource. It will then split the MariaDB reads into
        # that many partitions. Example: If we feed lowerBound=2024-01-01 and upperBound=2025-01-01, and
        # numPartitions = 12, then Spark will automatically split the reads over  'monthly' partitions, but
        # present back the one dataframe. So if over that year we had an even 12,000 rows, then each partition,
        # when picked up by an executor, will read ~1000 rows. Thus we can control the load each executor puts into
        # MariaDB, but also, the mem requirements of each executor.
        # More info from Spark docs: https://spark.apache.org/docs/3.5.1/sql-data-sources-jdbc.html#data-source-option
        opts.update({
            "partitionColumn": bound_column,
            "lowerBound": lower_bound,
            "upperBound": upper_bound,
            "numPartitions": num_partitions,
        })

    if where_clause:
        where_clause = f"{where_clause} AND {bound_column_filter}"
    else:
        where_clause = f"WHERE {bound_column_filter}"

    update_query_and_log(f"""
         {select_clause}
         FROM {table} {force_index}
         {where_clause}
        """)
    return spark.read.format("mediawiki-jdbc").options(**opts).load()


def find_inconsistent_rows(
        spark,
        wiki_id,
        target_table,
        results_table,
        min_timestamp,
        max_timestamp,
        computation_class,
        computation_dt,
        mariadb_args
) -> None:

    # Delete old runs for this (wiki_id, computation_class, computation_dt), if any, so that we can re-run successfully.
    delete_mismatch_rows_sql = f"""
DELETE
FROM {results_table}
WHERE wiki_id = '{wiki_id}'
  AND computation_class = '{computation_class}'
  AND computation_dt = TIMESTAMP '{computation_dt}'
    """

    print("Now doing DELETE of old inconsistency data via:")
    print(delete_mismatch_rows_sql)

    spark.sql(
        delete_mismatch_rows_sql
    ).collect()

    #
    # Fetch all revisions in a time window from a particular wiki_id,
    # NOTE: we have to select rev_timestamp as-is to allow spark to split and use indices
    #       and we also cast it as a datetime and rename it revision_dt for convenience later
    #
    source_revisions_sql_df = df_from_mariadb_replica_adaptive(
        spark,
        """
        SELECT rev_id                          AS revision_id,
               rev_page                        AS page_id,
               CAST(rev_timestamp as DATETIME) AS revision_dt,
               rev_timestamp,
               rev_deleted & 1 = 0             AS revision_content_is_visible,
               rev_deleted & 2 = 0             AS user_is_visible,
               rev_deleted & 4 = 0             AS revision_comment_is_visible,
               rev_len                         AS revision_size,
               rev_sha1                        AS revision_sha1
        """,
        None,
        "revision",
        "rev_timestamp",
        min_timestamp,
        max_timestamp,
        wiki_id,
        mariadb_args
    )
    from pyspark.storagelevel import StorageLevel
    # we want to cache source_revisions_sql_df as hitting the db is expensive and we want to do multiple transforms.
    # StorageLevel.DISK_ONLY, as the name implies, persist the df on local disk.
    # It requires significantly less memory on executors than default of StorageLevel.MEMORY_AND_DISK_DESER.
    source_revisions_sql_df.persist(StorageLevel.DISK_ONLY)
    # force a count so that we persist the df
    count = source_revisions_sql_df.count()
    print(f"Fetched {count} rows.")

    source_revisions_sql_df.createOrReplaceTempView("source_revisions")

    # again have to select both
    source_pages_deleted_df = df_from_mariadb_replica_adaptive(
        spark,
        """
        SELECT distinct log_page AS page_id
        """,
        """
        WHERE log_type = 'delete'
          AND log_action = 'delete'
        """,
        "logging",
        "log_timestamp",
        min_timestamp,
        max_timestamp,
        wiki_id,
        mariadb_args,
        force_index_name="log_page_id_time",
        partition_by_bound_column=False,
    )
    source_pages_deleted_df.createOrReplaceTempView("source_pages_deleted")

    source_pages_restored_df = df_from_mariadb_replica_adaptive(
        spark,
        """
        SELECT distinct log_page AS page_id
        """,
        """
        WHERE log_type = 'delete'
          AND log_action = 'restore'
        """,
        "logging",
        "log_timestamp",
        min_timestamp,
        max_timestamp,
        wiki_id,
        mariadb_args,
        force_index_name="log_page_id_time",
        partition_by_bound_column=False,
    )
    source_pages_restored_df.createOrReplaceTempView("source_pages_restored")

    target_revisions_sql = f"""
SELECT page_id,
       revision_id,
       revision_dt,
       revision_content_is_visible,
       user_is_visible,
       revision_comment_is_visible,
       revision_size,
       revision_sha1
FROM {target_table}
WHERE wiki_id = '{wiki_id}'
  AND revision_dt >= TIMESTAMP '{min_timestamp}'
  AND revision_dt  < TIMESTAMP '{max_timestamp}'
    """

    print(f"Fetching target revisions from a time window of {wiki_id}:")
    print(target_revisions_sql)

    target_revisions_sql_df = spark.sql(target_revisions_sql)
    target_revisions_sql_df.createOrReplaceTempView("target_revisions")

    emit_mismatch_rows_sql = f"""
SELECT '{wiki_id}'                                          AS wiki_id,
       COALESCE(t.page_id, s.page_id)                       AS page_id,
       revision_id,
       COALESCE(t.revision_dt, s.revision_dt) AS revision_dt,
       FILTER(
           ARRAY(
               IF(s.user_is_visible != t.user_is_visible, 'mismatch_user_visibility', NULL),
               IF(s.revision_content_is_visible != t.revision_content_is_visible, 'mismatch_content_visibility', NULL),
               IF(s.revision_comment_is_visible != t.revision_comment_is_visible, 'mismatch_comment_visibility', NULL),
               IF(t.revision_content_is_visible AND s.revision_sha1 != t.revision_sha1, 'mismatch_sha1', NULL),
               IF(s.revision_size != t.revision_size, 'mismatch_size', NULL),
               IF(s.page_id != t.page_id, 'mismatch_page', NULL),
               IF(s.page_id IS NULL, 'missing_from_source', NULL),
               IF(t.page_id IS NULL, 'missing_from_target', NULL),
               IF(d.page_id IS NOT NULL, 'page_was_deleted', NULL),
               IF(r.page_id IS NOT NULL, 'page_was_restored', NULL)
           ),
           reason -> reason IS NOT NULL
       )                                                    AS reasons,
       CAST('{computation_dt}' AS TIMESTAMP)                AS computation_dt,
       '{computation_class}'                                AS computation_class

FROM source_revisions s
ANTI JOIN source_pages_deleted spd USING (page_id)
FULL OUTER JOIN target_revisions t USING (revision_id)
LEFT JOIN source_pages_deleted  d ON (COALESCE(t.page_id, s.page_id) = d.page_id)
LEFT JOIN source_pages_restored r ON (COALESCE(t.page_id, s.page_id) = r.page_id)

WHERE s.user_is_visible != t.user_is_visible
   OR s.revision_content_is_visible != t.revision_content_is_visible
   OR s.revision_comment_is_visible != t.revision_comment_is_visible
   OR (t.revision_content_is_visible AND s.revision_sha1 != t.revision_sha1)
   OR s.revision_size != t.revision_size
   OR s.page_id != t.page_id
   OR s.page_id IS NULL
   OR t.page_id IS NULL
    """

    print("Calculating inconsistent rows via:")
    print(emit_mismatch_rows_sql)

    emit_mismatch_rows_sql_df = spark.sql(emit_mismatch_rows_sql)
    emit_mismatch_rows_sql_df.createOrReplaceTempView("results")

    insert_mismatch_rows_sql = f"""
INSERT INTO {results_table} (
       wiki_id,
       page_id,
       revision_id,
       revision_dt,
       reasons,
       computation_dt,
       computation_class,
       reconcile_emit_dt
)
SELECT wiki_id,
       page_id,
       revision_id,
       revision_dt,
       reasons,
       computation_dt,
       computation_class,
       NULL  -- reconcile_emit_dt
FROM results
    """

    print("Now doing INSERT via:")
    print(insert_mismatch_rows_sql)

    spark.sql(
        insert_mismatch_rows_sql
    ).collect()


def main() -> None:  # pragma: no cover
    args = parse_args(sys.argv[1:])

    spark = SparkSession.builder.getOrCreate()

    host, port = resolve_mariadb_host_port_for_wikidb(args.wiki_id)
    username = args.mariadb_username
    pw = resolve_pw_for_analytics_mariadb_replicas(args.mariadb_password_file)
    mariadb_args = {
        "host": host,
        "port": port,
        "username": username,
        "pw": pw,
    }

    find_inconsistent_rows(
        spark,
        args.wiki_id,
        args.target_table,
        args.results_table,
        args.min_timestamp,
        args.max_timestamp,
        args.computation_class,
        args.computation_dt,
        mariadb_args
    )

    spark.stop()
