import argparse
import sys

from pyspark.sql import SparkSession

from .replicas_util import resolve_mariadb_host_port_for_wikidb, resolve_pw_for_analytics_mariadb_replicas


def parse_args(args) -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Creates, formats and submits events to Kafka from a table that has a log of inconsistent rows. '
                    'This script works iteratively in chunks of the specified size until all events are emitted for a '
                    'particular inconsistency computation.'
    )
    parser.add_argument('--wiki_id', help='wiki_id to read inconsistency results from.', required=True)
    parser.add_argument('--inconsistent_rows_table', help='data lake table to read inconsistency results from.', required=True)
    parser.add_argument('--wikis_table', help='Table with canonical wiki info. Typically "canonical_data.wikis".', required=True)
    parser.add_argument('--namespaces_table', help='Table with canonical namespace info. Typically "wmf_raw.mediawiki_project_namespace_map".', required=True)
    parser.add_argument('--namespaces_table_snapshot', help='The Hive partition to be targeted for namespaces_table.', required=True)
    parser.add_argument('--computation_dt', help='The logical time to read inconsistency results from.', required=True)
    parser.add_argument('--computation_class', choices=['last-24h', 'all-of-wiki-time'],
                        help='This segregates between runs that cover one day of inconsistencies as of computation_dt, '
                             'versus runs that retroactively check all revisions as of computation_dt.',
                        required=True)
    parser.add_argument('--max_events_per_iteration', type=int,
                        help='The maximum amount of events to generate per event category (see code for categories) '
                             'in one iteration of the algorithm. This script will do as many iterations as needed '
                             'until all events for the specified computation_dt and computation_class are consumed.',
                        required=True)
    parser.add_argument('--event_stream_topic_prefix', help="Datacenter prefix like 'eqiad'. See https://wikitech.wikimedia.org/wiki/Kafka.", default='eqiad')
    parser.add_argument('--event_stream_name', help="The name of the stream to write to. Ex. 'mediawiki_content_history_reconcile'.", required=True)
    parser.add_argument('--event_schema_version', help="The version of the schema to validate against. Ex. '1.2.0'.", required=True)
    parser.add_argument('--kafka_bootstrap_servers', help="Comma separated list of addresses for the target Kafka serves. Ex 'kafka-jumbo1007.eqiad.wmnet:9092'.", required=True)
    parser.add_argument('--mariadb_username', help='The Analytics Replica MariaDB username.', required=True)
    parser.add_argument('--mariadb_password_file', help='The HDFS file path for the password to connect to the Analytics Replica MariaDB instance.', required=True)

    return parser.parse_args(args)


def df_from_mariadb_replica(
        spark,
        query,
        lower_bound,
        upper_bound,
        partition_column,
        num_partitions,
        wiki_id,
        host,
        port,
        user,
        password
):
    """
    Returns a DataFrame from a JDBC source.
    If partition_column is defined, then we leverage Spark's JDBC parallel reading / partitioning features.
    See https://spark.apache.org/docs/3.1.2/sql-data-sources-jdbc.html
    and https://medium.com/mercedes-benz-techinnovation-blog/increasing-apache-spark-read-performance-for-jdbc-connections-a028115e20cd
    """
    opts = {
        'driver' : 'com.mysql.cj.jdbc.Driver',
        'url': f'jdbc:mysql://{host}:{port}/{wiki_id}',
        'dbtable': f'({query}) t',
        'user': user,
        'password': password
    }

    if partition_column:
        opts.update({
            'partitionColumn': partition_column,
            'lowerBound': lower_bound,
            'upperBound': upper_bound,
            'numPartitions': num_partitions
        })

    return (spark.read
            .format("jdbc")
            .options(**opts)
            .load())


def truncated_print(s: str, limit: int = 2048) -> None:
    if len(s) > limit:
        print(s[:limit] + "...")
    else:
        print(s)


def emit_reconcile_events_to_kafka(
        spark,
        wiki_id,
        inconsistent_rows_table,
        wikis_table,
        namespaces_table,
        namespaces_table_snapshot,
        computation_dt,
        computation_class,
        max_events_per_iteration,
        mariadb_args,
        wmf_event_stream_args,
) -> int:
    """
    This algorithm works iteratively by consuming 'max_events_per_iteration' out of each category of inconsistencies
    as defined below: inconsistent revisions, deleted pages, and moved pages.
    The caller to this function should halt the iterations when we return 0 events processed.
    This is possible because the SQL statements check for "reconcile_emit_dt IS NULL" on each iteration, and at the end
    of this algorithm, we set that column to a non-null value for the rows processed in the current iteration. Thus,
    eventually, we will get 0 rows back from all SQL statements, and thus 0 events processed.
    """

    inconsistent_revision_ids_sql = f"""
SELECT revision_id
FROM {inconsistent_rows_table}
WHERE wiki_id = '{wiki_id}'
  AND computation_dt = TIMESTAMP '{computation_dt}'
  AND computation_class = '{computation_class}'
  AND reconcile_emit_dt IS NULL
  AND NOT ARRAY_CONTAINS(reasons, 'missing_from_source')
  AND NOT ARRAY_CONTAINS(reasons, 'mismatch_page')
ORDER BY revision_id ASC
LIMIT {max_events_per_iteration}
    """

    deleted_page_ids_sql = f"""
SELECT page_id,
       MAX(revision_id) AS max_revision_id,
       MAX(revision_dt) AS max_revision_dt
FROM {inconsistent_rows_table}
WHERE wiki_id = '{wiki_id}'
  AND computation_dt = TIMESTAMP '{computation_dt}'
  AND computation_class = '{computation_class}'
  AND reconcile_emit_dt IS NULL
  AND ARRAY_CONTAINS(reasons, 'missing_from_source')
  AND NOT ARRAY_CONTAINS(reasons, 'mismatch_page')
GROUP BY page_id
ORDER BY page_id ASC
LIMIT {max_events_per_iteration}
    """

    moved_page_ids_sql = f"""
SELECT page_id,
       MAX(revision_id) AS max_revision_id
FROM {inconsistent_rows_table}
WHERE wiki_id = '{wiki_id}'
  AND computation_dt = TIMESTAMP '{computation_dt}'
  AND computation_class = '{computation_class}'
  AND reconcile_emit_dt IS NULL
  AND NOT ARRAY_CONTAINS(reasons, 'missing_from_source')
  AND ARRAY_CONTAINS(reasons, 'mismatch_page')
GROUP BY page_id
ORDER BY page_id ASC
LIMIT {max_events_per_iteration}
    """

    print("Now fetching revision_ids or page_ids that need to be reconciled via:")
    print(inconsistent_revision_ids_sql)
    print(deleted_page_ids_sql)
    print(moved_page_ids_sql)

    inconsistent_revision_ids_rows = spark.sql(
        inconsistent_revision_ids_sql
    ).collect()
    inconsistent_revision_ids_rows_count = len(inconsistent_revision_ids_rows)

    deleted_page_ids_rows = spark.sql(
        deleted_page_ids_sql
    ).collect()
    deleted_page_ids_rows_count = len(deleted_page_ids_rows)

    moved_page_ids_rows = spark.sql(
        moved_page_ids_sql
    ).collect()
    moved_page_ids_rows_count = len(moved_page_ids_rows)

    print(f"Fetched {inconsistent_revision_ids_rows_count} inconsistent revision_ids.")
    print(f"Fetched {deleted_page_ids_rows_count} deleted page_ids.")
    print(f"Fetched {moved_page_ids_rows_count} moved page_ids.")

    if inconsistent_revision_ids_rows_count == 0 and deleted_page_ids_rows_count == 0 and moved_page_ids_rows_count == 0:
        print("Exiting early as there is no more data to submit.")
        # return the amount of events processed so that caller can halt iterations if 0.
        return 0

    inconsistent_revision_ids = [str(row['revision_id']) for row in inconsistent_revision_ids_rows]
    moved_max_revision_ids = [str(row['max_revision_id']) for row in moved_page_ids_rows]

    def revisions_source_sql_for_rev_ids(rev_ids) -> str:
        return f"""
            SELECT
              p.page_id AS page_id,
              p.page_namespace AS page_namespace_id,
              CONVERT(p.page_title USING utf8mb4) AS page_title,
              CAST(a.actor_id AS SIGNED) AS user_id,                 -- CAST is a workaround until T368755 is solved.
              IF(LENGTH(CONVERT(a.actor_name USING utf8mb4)) > 0,
                 CONVERT(a.actor_name USING utf8mb4),
                 'Unknown user'                                      -- Default if actor_name is empty. T382645.
              ) AS user_text,
              r.rev_deleted & 2 = 0 AS user_is_visible,
              CAST(r.rev_id AS SIGNED) AS revision_id,               -- CAST is a workaround until T368755 is solved.
              CAST(r.rev_parent_id AS SIGNED) AS revision_parent_id, -- CAST is a workaround until T368755 is solved.
              r.rev_timestamp AS mw_revision_dt,
              r.rev_minor_edit > 0 AS revision_is_minor_edit,
              CONVERT(c.comment_text USING utf8mb4) AS revision_comment,
              r.rev_deleted & 4 = 0 AS revision_comment_is_visible,
              CONVERT(r.rev_sha1 USING utf8mb4) AS revision_sha1,
              r.rev_len AS revision_size,
              CONVERT(sr.role_name USING utf8mb4) AS slot_role_name,
              CONVERT(cm.model_name USING utf8mb4) AS slot_content_model,
              CONVERT(ct.content_sha1 USING utf8mb4) AS slot_content_sha1,
              ct.content_size AS slot_content_size,
              r.rev_deleted & 1 = 0 AS revision_content_is_visible

            FROM revision r
            INNER JOIN comment c ON r.rev_comment_id = c.comment_id
            INNER JOIN page p ON r.rev_page = p.page_id
            INNER JOIN actor a ON r.rev_actor = a.actor_id
            INNER JOIN slots s ON r.rev_id = s.slot_revision_id
            INNER JOIN slot_roles sr ON s.slot_role_id = sr.role_id
            INNER JOIN content ct ON s.slot_content_id = ct.content_id
            INNER JOIN content_models cm ON ct.content_model = cm.model_id

            WHERE r.rev_id IN ({', '.join(rev_ids)})
        """

    print("Now figuring latest state of inconsistent revisions via:")
    truncated_print(revisions_source_sql_for_rev_ids(inconsistent_revision_ids or ["-1"]))

    inconsistent_revision_ids_df = df_from_mariadb_replica(
        spark,
        revisions_source_sql_for_rev_ids(inconsistent_revision_ids or ["-1"]),
        None,
        None,
        None,
        None,
        wiki_id,
        mariadb_args['host'],
        mariadb_args['port'],
        mariadb_args['username'],
        mariadb_args['pw'],
    )

    inconsistent_revision_ids_df.createOrReplaceTempView("flat_inconsistent_revision_ids_source")

    print("Now figuring latest state of moved max revision ids via:")
    truncated_print(revisions_source_sql_for_rev_ids(moved_max_revision_ids or ["-1"]))

    moved_max_revision_ids_df = df_from_mariadb_replica(
        spark,
        revisions_source_sql_for_rev_ids(moved_max_revision_ids or ["-1"]),
        None,
        None,
        None,
        None,
        wiki_id,
        mariadb_args['host'],
        mariadb_args['port'],
        mariadb_args['username'],
        mariadb_args['pw'],
    )
    moved_max_revision_ids_df.createOrReplaceTempView("flat_moved_max_revision_ids_source")

    transform_revisions_to_events_sql = f"""
WITH revisions_source AS (
  SELECT
    'update' AS changelog_kind,
    'edit' AS page_change_kind,
    FIRST(page_id) AS page_id,
    FIRST(page_namespace_id) AS page_namespace_id,
    FIRST(page_title) AS page_title_no_ns,
    FIRST(user_id) AS user_id,
    FIRST(user_text) AS user_text,
    FIRST(user_is_visible) AS user_is_visible,
    revision_id,
    FIRST(revision_parent_id) AS revision_parent_id,
    FIRST(TO_TIMESTAMP(mw_revision_dt, 'yyyyMMddHHmmss')) AS revision_dt,
    FIRST(revision_is_minor_edit) AS revision_is_minor_edit,
    FIRST(revision_comment) AS revision_comment,
    FIRST(revision_comment_is_visible) AS revision_comment_is_visible,
    FIRST(revision_sha1) AS revision_sha1,
    FIRST(revision_size) AS revision_size,
    MAP_FROM_ENTRIES(
      COLLECT_LIST(
        STRUCT(
          slot_role_name, NAMED_STRUCT(
                          'content_body', NULL,    -- not persisted in db, enrich will fetch.
                          'content_format', NULL,  -- not persisted in db, enrich will fetch.
                          'content_model', slot_content_model,
                          'content_sha1', slot_content_sha1,
                          'content_size', slot_content_size,
                          'origin_rev_id', revision_id,
                          'slot_role', slot_role_name
                        )
        )
      ) 
    ) as content_slots,
    FIRST(revision_content_is_visible) AS revision_content_is_visible
  FROM  flat_inconsistent_revision_ids_source
  GROUP BY
    revision_id
),
page_deletes_source AS (
  SELECT
    'delete' AS changelog_kind,
    'delete' AS page_change_kind,
    ds.page_id AS page_id,
    0 AS page_namespace_id, -- this is just to make a later join to namespaces_table work; the number itself is meaninless.
                            -- it has to be >= 0 for page change schema to accept the event.
    'dummy' AS page_title_no_ns,
    NULL AS user_id,
    NULL AS user_text,
    NULL AS user_is_visible,
    ds.max_revision_id AS revision_id,
    NULL AS revision_parent_id,
    ds.max_revision_dt AS revision_dt,
    NULL AS revision_is_minor_edit,
    NULL AS revision_comment,
    NULL AS revision_comment_is_visible,
    NULL AS revision_sha1,
    NULL AS revision_size,
    NULL AS content_slots,
    NULL AS revision_content_is_visible
  FROM (
    {deleted_page_ids_sql}
  ) ds
),
page_moves_source AS (
  SELECT
    'update' AS changelog_kind,
    'move' AS page_change_kind,
    FIRST(page_id) AS page_id,
    FIRST(page_namespace_id) AS page_namespace_id,
    FIRST(page_title) AS page_title_no_ns,
    FIRST(user_id) AS user_id,
    FIRST(user_text) AS user_text,
    FIRST(user_is_visible) AS user_is_visible,
    revision_id,
    FIRST(revision_parent_id) AS revision_parent_id,
    FIRST(TO_TIMESTAMP(mw_revision_dt, 'yyyyMMddHHmmss')) AS revision_dt,
    FIRST(revision_is_minor_edit) AS revision_is_minor_edit,
    FIRST(revision_comment) AS revision_comment,
    FIRST(revision_comment_is_visible) AS revision_comment_is_visible,
    FIRST(revision_sha1) AS revision_sha1,
    FIRST(revision_size) AS revision_size,
    MAP_FROM_ENTRIES(
      COLLECT_LIST(
        STRUCT(
          slot_role_name, NAMED_STRUCT(
                          'content_body', NULL,    -- not persisted in db, enrich will fetch.
                          'content_format', NULL,  -- not persisted in db, enrich will fetch.
                          'content_model', slot_content_model,
                          'content_sha1', slot_content_sha1,
                          'content_size', slot_content_size,
                          'origin_rev_id', revision_id,
                          'slot_role', slot_role_name
                        )
        )
      ) 
    ) as content_slots,
    FIRST(revision_content_is_visible) AS revision_content_is_visible
  FROM  flat_moved_max_revision_ids_source
  GROUP BY
    revision_id
)

SELECT '/mediawiki/page/change/1.2.0' AS `$schema`,
       s.changelog_kind AS changelog_kind,
       s.revision_comment AS comment,
       DATE_FORMAT(s.revision_dt, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") AS dt,
       NAMED_STRUCT(
           'stream', '{wmf_event_stream_args['event_stream_name']}',
           'domain', cd.domain_name
       ) AS meta,
       NAMED_STRUCT(
           'namespace_id', s.page_namespace_id,
           'page_id', s.page_id,
           'page_title', IF(COALESCE(ns.namespace_localized_name, '') = '',
                            s.page_title_no_ns,
                            CONCAT(ns.namespace_localized_name, ':', s.page_title_no_ns)
                         )
       ) AS page,
       -- page_change_kind is innacurate, and included just to comply with schema
       s.page_change_kind AS page_change_kind,
       NAMED_STRUCT(
           'comment', s.revision_comment,
           'content_slots', s.content_slots,
           'editor', NAMED_STRUCT(
                         'user_id', s.user_id,
                         'user_text', s.user_text
                     ),
           'is_comment_visible', s.revision_comment_is_visible > 0,
           'is_content_visible', s.revision_content_is_visible > 0,
           'is_editor_visible',  s.user_is_visible > 0,
           'is_minor_edit', s.revision_is_minor_edit > 0,
           'rev_dt', DATE_FORMAT(s.revision_dt, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"),
           'rev_id', s.revision_id,
           'rev_parent_id', s.revision_parent_id,
           'rev_sha1', s.revision_sha1,
           'rev_size', s.revision_size
       ) as revision,
       '{wiki_id}' AS wiki_id
FROM (
    SELECT * FROM revisions_source
    UNION ALL
    SELECT * FROM page_deletes_source
    UNION ALL
    SELECT * FROM page_moves_source
) s
INNER JOIN {wikis_table} cd ON ('{wiki_id}' = cd.database_code)  -- INNER so that if no join, no event
INNER JOIN {namespaces_table} ns ON (                            -- INNER so that if no join, no event
  '{wiki_id}' = ns.dbname AND s.page_namespace_id = ns.namespace AND ns.snapshot = '{namespaces_table_snapshot}'
)
    """

    print("Now transforming latest state of revisions to events that follow the mediawiki_page_change schema via:")
    print(transform_revisions_to_events_sql)

    events_df = spark.sql(transform_revisions_to_events_sql)
    events_df.createOrReplaceTempView("events")  # temp view is used when debugging code on a notebook.

    print(f"Now emitting events to Kafka via wmf-event-stream.")

    (events_df
        .write
        .format("wmf-event-stream")
        .option("event-stream-topic-prefix", wmf_event_stream_args['event_stream_topic_prefix'])
        .option("event-stream-name", wmf_event_stream_args['event_stream_name'])
        .option("event-schema-version", wmf_event_stream_args['event_schema_version'])
        .option("event-schema-base-uris", "https://schema.wikimedia.org/repositories/primary/jsonschema")
        .option("event-stream-config-uri", "https://meta.wikimedia.org/w/api.php?action=streamconfigs")
        .option("kafka.bootstrap.servers", wmf_event_stream_args['kafka_bootstrap_servers'])
        .save()
     )

    # if we are still here, it means we did not except, thus we assume that wmf-event-stream was successful.
    # let's now mark the inconsistent rows as submitted.
    # yes, this is not transactional, it is all right if we double submit.

    update_inconsistent_rows_sql = f"""
UPDATE {inconsistent_rows_table}
SET reconcile_emit_dt = NOW()
WHERE wiki_id = '{wiki_id}'
  AND computation_dt = TIMESTAMP '{computation_dt}'
  AND computation_class = '{computation_class}'
  AND reconcile_emit_dt IS NULL
  AND (
    revision_id IN (SELECT revision_id FROM ({inconsistent_revision_ids_sql}))
    OR
    page_id IN (SELECT page_id FROM ({deleted_page_ids_sql}))
    OR
    page_id IN (SELECT page_id FROM ({moved_page_ids_sql}))
  )
    """

    print("Now doing UPDATE via:")
    print(update_inconsistent_rows_sql)

    spark.sql(
        update_inconsistent_rows_sql
    ).collect()

    # return the amount of events processed so that caller can halt iterations if 0.
    return inconsistent_revision_ids_rows_count + deleted_page_ids_rows_count + moved_page_ids_rows_count


def main() -> None:  # pragma: no cover
    args = parse_args(sys.argv[1:])

    spark = SparkSession.builder.getOrCreate()

    host, port = resolve_mariadb_host_port_for_wikidb(args.wiki_id)
    username = args.mariadb_username
    pw = resolve_pw_for_analytics_mariadb_replicas(args.mariadb_password_file)
    mariadb_args = {
        "host": host,
        "port": port,
        "username": username,
        "pw": pw,
    }

    wmf_event_stream_args = {
        "event_stream_topic_prefix": args.event_stream_topic_prefix,
        "event_stream_name": args.event_stream_name,
        "event_schema_version": args.event_schema_version,
        "kafka_bootstrap_servers": args.kafka_bootstrap_servers,
    }

    done = False
    while not done:
        events_processed =\
            emit_reconcile_events_to_kafka(
                spark,
                args.wiki_id,
                args.inconsistent_rows_table,
                args.wikis_table,
                args.namespaces_table,
                args.namespaces_table_snapshot,
                args.computation_dt,
                args.computation_class,
                args.max_events_per_iteration,
                mariadb_args,
                wmf_event_stream_args,
            )
        print(f"Events processed on this iteration: {events_processed}.")
        if events_processed == 0:
            done = True

    spark.stop()
