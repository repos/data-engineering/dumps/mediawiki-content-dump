import argparse
import sys

from pyspark.sql import SparkSession


def parse_args(args) -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Run various SQL DML from an event source table to an Iceberg sink table.'
    )
    parser.add_argument('--source_table', help='table we intend to read from')
    parser.add_argument('--target_table', help='table we intend to write to')
    parser.add_argument('--year', type=int, help='int year from source_table to be ingested')
    parser.add_argument('--month', type=int, help='int month from source_table to be ingested')
    parser.add_argument('--day', type=int, help='int day from source_table to be ingested')
    parser.add_argument('--hour', type=int, help='int hour from source_table to be ingested. If not provided, will ingest all day.')
    parser.add_argument('--event_categories', choices=['revisions', 'page_deletes', 'page_moves'], nargs='+',
                        help='categories of events to process. Choose one or more. Will map to specific method calls.')

    return parser.parse_args(args)


def process_revisions(spark, source_table, target_table, year, month, day, hour) -> None:

    sql_earliest_revision_dt_per_wikis = \
        f"""
SELECT
  wiki_id,
  MIN(to_timestamp(revision.rev_dt)) as earliest_revision_dt
FROM {source_table}
WHERE year={year}
  AND month={month}
  AND day={day}
  {f"AND hour={hour}" if hour else ""}
AND page_change_kind IN ('create', 'edit')
GROUP BY wiki_id
ORDER BY wiki_id, earliest_revision_dt
        """

    print("Now calculating earliest revision timestamps per wiki to process via:")
    print(sql_earliest_revision_dt_per_wikis)

    earliest_revision_dt_per_wikis = spark.sql(
        sql_earliest_revision_dt_per_wikis
    ).collect()

    print(f"There are {len(earliest_revision_dt_per_wikis)} wikis to process.")

    if not earliest_revision_dt_per_wikis:
        print("Exiting process_revisions() early as there is no data to process.")
        return

    optimization_predicates = '\nOR\n'.join([
        f"(t.wiki_id = '{row['wiki_id']}' AND t.revision_dt >= '{row['earliest_revision_dt'].isoformat()}')"
        for row in earliest_revision_dt_per_wikis
    ])

    sql_merge_into = \
        f"""
WITH deduplicated_mediawiki_page_content_change AS (
    -- we want to ingest the latest state of a (wiki_id, rev_id) pair
    -- so below we pick latest event based on dt (event timestamp).
    -- if there are multiple events with same dt, pick highest changelog_priority
    SELECT * FROM (
        SELECT
            *,
            row_number() over ( PARTITION BY wiki_id, revision.rev_id ORDER BY to_timestamp(dt) DESC, changelog_priority ASC ) AS row_num
        FROM (
            SELECT
                *,
                CASE
                    WHEN changelog_kind = 'update' THEN 1
                    WHEN changelog_kind = 'insert' THEN 2
                END AS changelog_priority
            FROM {source_table}
            WHERE year={year}
              AND month={month}
              AND day={day}
              {f"AND hour={hour}" if hour else ""}
              AND page_change_kind IN ('create', 'edit')
        )
    )
    WHERE row_num = 1
)

MERGE INTO {target_table} t
USING (
  SELECT
    -- fields that will be part of the dump
    page.page_id                        AS s_page_id,
    page.namespace_id                   AS s_page_namespace_id,
    page.page_title                     AS s_page_title,
    created_redirect_page.page_title    AS s_page_redirect_target,
    performer.user_id                   AS s_user_id,
    performer.user_text                 AS s_user_text,
    revision.is_editor_visible          AS s_user_is_visible,
    revision.rev_id                     AS s_revision_id,
    revision.rev_parent_id              AS s_revision_parent_id,
    to_timestamp(revision.rev_dt)       AS s_revision_dt,
    revision.is_minor_edit              AS s_revision_is_minor_edit,
    revision.comment                    AS s_revision_comment,
    revision.is_comment_visible         AS s_revision_comment_is_visible,
    revision.rev_sha1                   AS s_revision_sha1,
    revision.rev_size                   AS s_revision_size,
    transform_values(revision.content_slots,
                     (k, v) -> (
                        v.content_body,
                        v.content_format,
                        v.content_model,
                        v.content_sha1,
                        v.content_size
                     )
                    )                   AS s_revision_content_slots,
    revision.is_content_visible         AS s_revision_content_is_visible,
    wiki_id                             AS s_wiki_id,

    -- fields that help us control the ingestion
    changelog_kind                      AS s_changelog_kind,
    to_timestamp(meta.dt)               AS s_meta_dt,
    meta.id                             AS s_meta_id
  FROM deduplicated_mediawiki_page_content_change s
  )

ON  s_wiki_id = t.wiki_id
AND s_revision_id = t.revision_id
-- pushdown wiki_ids that are changing
-- to limit how much data we effectively read
AND (
{optimization_predicates}
)

WHEN MATCHED AND s_changelog_kind IN ('insert', 'update') AND s_meta_dt >= t.row_content_update_dt THEN
  UPDATE SET
    t.page_id = s_page_id,
    t.page_namespace_id = s_page_namespace_id,
    t.page_title = s_page_title,
    t.page_redirect_target = s_page_redirect_target,
    t.user_id = s_user_id,
    t.user_text = s_user_text,
    t.revision_id = s_revision_id,
    t.revision_parent_id = s_revision_parent_id,
    t.revision_dt = s_revision_dt,
    t.revision_is_minor_edit = s_revision_is_minor_edit,
    t.revision_comment = s_revision_comment,
    t.revision_sha1 = s_revision_sha1,
    t.revision_size = s_revision_size,
    t.revision_content_slots = s_revision_content_slots,
    t.wiki_id = s_wiki_id,
    t.row_content_update_dt = s_meta_dt
WHEN NOT MATCHED AND s_changelog_kind IN ('insert', 'update') THEN
  INSERT (
    page_id,
    page_namespace_id,
    page_title,
    page_redirect_target,
    user_id,
    user_text,
    user_is_visible,
    revision_id,
    revision_parent_id,
    revision_dt,
    revision_is_minor_edit,
    revision_comment,
    revision_comment_is_visible,
    revision_sha1,
    revision_size,
    revision_content_slots,
    revision_content_is_visible,
    wiki_id,
    row_content_update_dt,
    row_visibility_update_dt,
    row_move_update_dt
  ) VALUES (
    s_page_id,
    s_page_namespace_id,
    s_page_title,
    s_page_redirect_target,
    s_user_id,
    s_user_text,
    s_user_is_visible,
    s_revision_id,
    s_revision_parent_id,
    s_revision_dt,
    s_revision_is_minor_edit,
    s_revision_comment,
    s_revision_comment_is_visible,
    s_revision_sha1,
    s_revision_size,
    s_revision_content_slots,
    s_revision_content_is_visible,
    s_wiki_id,
    s_meta_dt,
    s_meta_dt,
    s_meta_dt
  )
        """

    print("Now doing MERGE INTO via:")
    print(sql_merge_into)

    spark.sql(
        sql_merge_into
    ).collect()


def process_page_deletes(spark, source_table, target_table, year, month, day, hour) -> None:

    sql_page_ids_per_wiki = \
        f"""
SELECT
  wiki_id,
  sort_array(collect_set(page_id)) as page_ids
FROM (
  SELECT
    wiki_id,
    page.page_id
  FROM {source_table}
  WHERE year={year}
    AND month={month}
    AND day={day}
    {f"AND hour={hour}" if hour else ""}
    AND page_change_kind = 'delete'
)
GROUP BY wiki_id
ORDER BY wiki_id
        """

    print("Now calculating page_ids to delete via:")
    print(sql_page_ids_per_wiki)

    page_ids_per_wiki = spark.sql(
        sql_page_ids_per_wiki
    ).collect()

    print(f"There are {len(page_ids_per_wiki)} wikis to process.")

    if not page_ids_per_wiki:
        print("Exiting process_page_deletes() early as there is no data to process.")
        return

    delete_predicates = '\nOR\n'.join([
        f"(t.wiki_id = '{pipw['wiki_id']}' AND t.page_id IN ({', '.join(map(str, pipw['page_ids']))}))"
        for pipw in page_ids_per_wiki
    ])

    sql_delete = \
        f"""
DELETE
FROM {target_table} t
WHERE (
-- pushdown wiki_ids and page_ids that are changing
-- to limit how much data we effectively read. See T369868#10142190.
{delete_predicates}
)
        """

    print("Now doing DELETE via:")
    print(sql_delete)

    spark.sql(
        sql_delete
    ).collect()


def process_page_moves(spark, source_table, target_table, year, month, day, hour) -> None:

    sql_page_ids_per_wiki = \
        f"""
SELECT wiki_id,
       sort_array(collect_set(page_id)) as page_ids
FROM (
    SELECT
        wiki_id,
        page.page_id
    FROM {source_table}
    WHERE year={year}
      AND month={month}
      AND day={day}
      {f"AND hour={hour}" if hour else ""}
      AND page_change_kind = 'move'
)
GROUP BY wiki_id
ORDER BY wiki_id
        """

    print("Now calculating page_ids to move via:")
    print(sql_page_ids_per_wiki)

    page_ids_per_wiki = spark.sql(
        sql_page_ids_per_wiki
    ).collect()

    print(f"There are {len(page_ids_per_wiki)} wikis to process.")

    if not page_ids_per_wiki:
        print("Exiting process_page_moves() early as there is no data to process.")
        return

    optimization_predicates = '\nOR\n'.join([
        f"(t.wiki_id = '{pipw['wiki_id']}' AND t.page_id IN ({', '.join(map(str, pipw['page_ids']))}))"
        for pipw in page_ids_per_wiki
    ])

    sql_merge_into = \
        f"""
WITH deduplicated_mediawiki_page_moves AS (
    -- pick latest event for a particular revision_id based on dt (event timestamp).
    SELECT * FROM (
        SELECT
          t.wiki_id,
          t.revision_id,
          s.page.namespace_id AS page_namespace_id,
          s.page.page_title,
          to_timestamp(s.meta.dt) as s_meta_dt,
          row_number() over ( PARTITION BY t.wiki_id, t.revision_id ORDER BY to_timestamp(s.dt) DESC) AS row_num
        FROM {target_table} t
        JOIN {source_table} s ON ( t.wiki_id = s.wiki_id AND t.page_id = s.page.page_id )
        WHERE s.year={year}
          AND s.month={month}
          AND s.day={day}
          {f"AND s.hour={hour}" if hour else ""}
          AND s.page_change_kind = 'move'
          -- pushdown wiki_ids and page_ids that are changing
          -- to limit how much data we effectively read. See T369868#10142190.
          AND (
        {optimization_predicates}
        )
    )
    WHERE row_num = 1
)

MERGE INTO {target_table} t
USING (
  SELECT
    s.wiki_id,
    s.revision_id,
    s.page_namespace_id,
    s.page_title,
    s.s_meta_dt
  FROM deduplicated_mediawiki_page_moves s
)

ON  s.wiki_id = t.wiki_id
AND s.revision_id = t.revision_id
-- pushdown wiki_ids and page_ids that are changing
-- to limit how much data we effectively read. See T369868#10142190.
AND (
{optimization_predicates}
)
                                         -- row_move_update_dt was introduced '2024-09-17'
WHEN MATCHED AND s_meta_dt >= COALESCE(t.row_move_update_dt, TIMESTAMP '2024-09-17')  THEN
  UPDATE SET
    t.page_namespace_id = s.page_namespace_id,
    t.page_title = s.page_title,
    t.row_move_update_dt = s_meta_dt
        """

    print("Now doing MERGE INTO via:")
    print(sql_merge_into)

    spark.sql(
        sql_merge_into
    ).collect()


def main() -> None:  # pragma: no cover
    args = parse_args(sys.argv[1:])

    spark = SparkSession.builder.getOrCreate()

    if 'revisions' in args.event_categories:
        process_revisions(
            spark,
            args.source_table,
            args.target_table,
            args.year,
            args.month,
            args.day,
            args.hour
        )

    if 'page_deletes' in args.event_categories:
        process_page_deletes(
            spark,
            args.source_table,
            args.target_table,
            args.year,
            args.month,
            args.day,
            args.hour
        )

    if 'page_moves' in args.event_categories:
        process_page_moves(
            spark,
            args.source_table,
            args.target_table,
            args.year,
            args.month,
            args.day,
            args.hour
        )

    spark.stop()
