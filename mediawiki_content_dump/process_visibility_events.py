import argparse
import sys

from pyspark.sql import SparkSession


def parse_args(args) -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Run a MERGE INTO from a visibility event source table to an Iceberg sink table.'
    )
    parser.add_argument('--source_table', help='table we intend to read from')
    parser.add_argument('--target_table', help='table we intend to write to')
    parser.add_argument('--year', type=int, help='int year from source_table to be ingested')
    parser.add_argument('--month', type=int, help='int month from source_table to be ingested')
    parser.add_argument('--day', type=int, help='int day from source_table to be ingested. If not provided, will ingest all day.')
    parser.add_argument('--hour', type=int, help='int hour from source_table to be ingested')

    return parser.parse_args(args)


def process_visibility_events(spark, source_table, target_table, year, month, day, hour) -> None:

    sql_rev_ids_per_page_ids_per_wiki = \
        f"""
SELECT
  database,
  page_id,
  sort_array(collect_set(rev_id)) as rev_ids
FROM {source_table}
WHERE year = {year}
  AND month = {month}
  AND day = {day}
  {f"AND hour = {hour}" if hour else ""}
GROUP BY database, page_id
ORDER BY database, page_id
        """

    print("Now calculating page_ids and rev_ids to modify via:")
    print(sql_rev_ids_per_page_ids_per_wiki)

    rev_ids_per_page_ids_per_wiki = spark.sql(
        sql_rev_ids_per_page_ids_per_wiki
    ).collect()

    print(f"There are {len(rev_ids_per_page_ids_per_wiki)} page_ids to process.")

    if not rev_ids_per_page_ids_per_wiki:
        print("Exiting process_visibility_events() early as there is no data to process.")
        return

    optimization_predicates = '\nOR\n'.join([
        f"(t.wiki_id = '{row['database']}' AND t.page_id = {row['page_id']} AND t.revision_id IN ({', '.join(map(str, row['rev_ids']))}))"
        for row in rev_ids_per_page_ids_per_wiki
    ])

    sql_merge_into =\
        f"""
WITH deduplicated_mediawiki_revision_visibility_change AS (
  SELECT *
  FROM (
    SELECT *,
           row_number() over ( PARTITION BY database, rev_id ORDER BY to_timestamp(rev_timestamp) DESC) AS row_num
    FROM {source_table}
    WHERE year = {year}
    AND month = {month}
    AND day = {day}
    {f"AND hour = {hour}" if hour else ""}
  )
  WHERE row_num = 1
)

MERGE INTO {target_table} t
USING (
  SELECT 
    visibility,
    rev_id,
    database,
    to_timestamp(meta.dt) AS meta_dt
  FROM deduplicated_mediawiki_revision_visibility_change) AS s

ON t.revision_id = s.rev_id
AND s.database = t.wiki_id
-- pushdown wiki_ids and page_ids and revision_ids that are changing
-- to limit how much data we effectively read.
AND (
{optimization_predicates}
)

WHEN MATCHED AND s.meta_dt >= t.row_visibility_update_dt THEN
    UPDATE SET
      t.revision_comment_is_visible = s.visibility.comment,
      t.user_is_visible = s.visibility.user,
      t.revision_content_is_visible = s.visibility.text,
      t.row_visibility_update_dt = s.meta_dt
        """

# We deliberately do not include a WHEN NOT MATCHED clause here.
# It could be the case that we miss updates that made it to the
# visibility stream before the mediawiki_page_content_change
# stream, but we opt for the simplicity of only adding new tuples
# while consuming mediawiki_page_content_change. On backfills, we will reconciliate.

    print("Now doing MERGE INTO via:")
    print(sql_merge_into)

    spark.sql(
        sql_merge_into
    ).collect()


def main() -> None:  # pragma: no cover
    args = parse_args(sys.argv[1:])

    spark = SparkSession.builder.getOrCreate()

    process_visibility_events(
        spark,
        args.source_table,
        args.target_table,
        args.year,
        args.month,
        args.day,
        args.hour
    )

    spark.stop()
