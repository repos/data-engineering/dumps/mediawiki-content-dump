import dns.resolver
import requests
import subprocess

import mediawiki_content_dump


def resolve_shard_for_wikidb(wikidb, datacenter='eqiad'):
    """
    Given a wikidb (i.e. 'simplewiki'), return the target shard that will contain that database.
    Datacenter can be optionally provided, but the Hadoop cluster so far is only available at eqiad.
    """
    if not hasattr(resolve_shard_for_wikidb, "wikidb_to_shard"):
        resolve_shard_for_wikidb.wikidb_to_shard = {}

        response = requests.get(url=f"https://noc.wikimedia.org/db.php?dc={datacenter}&format=json",
                                # proxies={"https": "http://webproxy.eqiad.wmnet:8080"},
                                headers={
                                    "User-Agent": f"mediawiki-content-dump/{mediawiki_content_dump.__version__} "
                                                  "(https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump)"
                                    },
                                timeout=60)
        data = response.json()
        for shard in data.keys():
            for db in data[shard]['dbs']:
                resolve_shard_for_wikidb.wikidb_to_shard[db] = shard

    if wikidb in resolve_shard_for_wikidb.wikidb_to_shard:
        return resolve_shard_for_wikidb.wikidb_to_shard[wikidb]
    else:
        return "s3"  # As per noc.wikimedia.org, any wiki not hosted on the other sections belongs to s3


def resolve_mariadb_host_port_for_wikidb(wikidb):
    """
    Given a wikidb (i.e. 'simplewiki'), return the host and port of the MariaDB instance that will contain a replica
    of that MediaWiki instance's tables.
    """
    shard = resolve_shard_for_wikidb(wikidb)
    answers = dns.resolver.resolve('_' + shard + '-analytics._tcp.eqiad.wmnet', 'SRV')
    host, port = str(answers[0].target), answers[0].port
    return host.rstrip('.'), port


def resolve_pw_for_analytics_mariadb_replicas(password_file):
    """
    Shells out to discover the password to be able to access the analytics mariadb replicas.
    The password file permissions must match the current user to succeed.
    """
    pw = subprocess.run(
        f"hdfs dfs -cat {password_file}",
        shell=True,
        stdout=subprocess.PIPE,
        universal_newlines=True
    ).stdout.strip()
    return pw
