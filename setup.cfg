# This file is used to configure your project.
# Read more about the various options under:
# https://setuptools.pypa.io/en/latest/userguide/declarative_config.html
# https://setuptools.pypa.io/en/latest/references/keywords.html

[metadata]
name = mediawiki-content-dump
description = PySpark project with data pipeline to generate MediaWiki Content Dumps
version = 0.4.0.dev
author = Xabriel J. Collazo Mojica
author_email = xcollazo@wikimedia.org
license = MIT
license_files = LICENSE.txt
long_description = file: README.rst
long_description_content_type = text/x-rst; charset=UTF-8
url = https://gitlab.wikimedia.org/xcollazo/mediawiki-content-dump

# Change if running only on Windows, Mac or Linux (comma-separated)
platforms = any

[options]
zip_safe = False
packages = find:
include_package_data = True

# Require a min/specific Python version (comma-separated conditions)
# python_requires = >=3.10

# Add here dependencies of your project (line-separated), e.g. requests>=2.2,<3.0.
# Version specifiers like >=2.2,<3.0 avoid problems due to API changes in
# new major versions. This works if the required packages follow Semantic Versioning.
# For more information, check out https://semver.org/.
install_requires =
    importlib-metadata; python_version<"3.10"

# Add here test requirements (semicolon/line-separated)
testing =
    setuptools
    pytest
    pytest-cov

[options.entry_points]
# Add here console scripts like:
# console_scripts =
#     script_name = mediawiki_content_dump.module:function
# For example:
# console_scripts =
#     fibonacci = mediawiki_content_dump.skeleton:run
# And any other entry points, for example:
# pyscaffold.cli =
#     awesome = pyscaffoldext.awesome.extension:AwesomeExtension
console_scripts =
    process_visibility_events.py = mediawiki_content_dump.process_visibility_events:main
    process_events.py = mediawiki_content_dump.process_events:main
    consistency_check.py = mediawiki_content_dump.consistency_check:main
    emit_reconcile_events_to_kafka.py = mediawiki_content_dump.emit_reconcile_events_to_kafka:main
    compute_metrics.py = mediawiki_content_dump.compute_metrics:main

[tool:pytest]
# Specify command line options as you would do when invoking pytest directly.
# e.g. --cov-report html (or xml) for html/xml output or --junitxml junit.xml
# in order to write a coverage file that can be read by Jenkins.
# CAUTION: --cov flags may prohibit setting breakpoints while debugging.
#          Comment those flags to avoid this pytest issue.
addopts =
    --cov mediawiki_content_dump --cov-report term-missing
    --verbose
norecursedirs =
    dist
    build
    .tox
testpaths = tests
# Use pytest markers to select/deselect specific tests
# markers =
#     slow: mark tests as slow (deselect with '-m "not slow"')
#     system: mark end-to-end system tests

[devpi:upload]
# Options for the devpi: PyPI server and packaging tool
# VCS export must be deactivated since we are using setuptools-scm
no_vcs = 1
formats = bdist_wheel

[flake8]
# Some sane defaults for the code style checker flake8
max_line_length = 88
extend_ignore = E203, W503
# ^  Black-compatible
#    E203 and W503 have edge cases handled by black
exclude =
    .tox
    build
    dist
    .eggs
    docs/conf.py

[pyscaffold]
# PyScaffold's parameters when the project was created.
# This will be used when updating. Do not change!
version = 4.5
package = mediawiki_content_dump
extensions =
    gitlab
    no_tox
    pre_commit
