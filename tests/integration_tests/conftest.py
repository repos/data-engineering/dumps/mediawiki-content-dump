import os
import time
import pytest
from pyspark.sql import SparkSession


@pytest.fixture(scope="session")
def spark():
    # set timezone to UTC globally.
    os.environ['TZ'] = 'UTC'
    time.tzset()
    # set spark's timezone to UTC as well
    spark_session = SparkSession.builder.config("spark.sql.session.timeZone", "UTC").getOrCreate()
    yield spark_session
    spark_session.stop()