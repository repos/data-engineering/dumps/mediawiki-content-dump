from mediawiki_content_dump.compute_metrics import parse_args


def test_parse_args():
    # Usage: compute_metrics.py --wiki_dbs enwiki jawiki
    #                           --content_table content.table
    #                           --inconsistent_rows_table inconsistent_rows.table
    #                           --metrics_table metrics.table
    #                           --min_timestamp 2024-12-21 00:00:00
    #                           --max_timestamp 2024-12-22 00:00:00
    #                           --run_id 1234
    args0 = ['--wiki_ids', 'enwiki,jawiki',
             '--content_table', 'content.table',
             '--inconsistent_rows_table', 'inconsistent_rows.table',
             '--metrics_table', 'metrics.table',
             '--min_timestamp', '2024-12-21T00:00:00',
             '--max_timestamp', '2024-12-22T00:00:00',
             '--run_id', '1234',
             ]
    parsed = parse_args(args0)
    print(parsed)
    assert parsed.wiki_ids == ['enwiki', 'jawiki']
    assert parsed.content_table == 'content.table'
    assert parsed.inconsistent_rows_table == 'inconsistent_rows.table'
    assert parsed.metrics_table == 'metrics.table'
    assert parsed.min_timestamp == '2024-12-21T00:00:00'
    assert parsed.max_timestamp == '2024-12-22T00:00:00'
    assert parsed.run_id == '1234'
