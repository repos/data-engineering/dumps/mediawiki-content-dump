from datetime import datetime
import pytest

from mediawiki_content_dump.consistency_check import parse_args


def test_parse_args():
    # Usage: consistency_check.py  --wiki_id enwiki
    #                              --target_table wmf_dumps.wikitext_raw_rc2
    #                              --results_table wmf_dumps.wikitext_mismatch_rows_rc1
    #                              --min_timestamp 2024-01-01TOO:OO:OO
    #                              --max_timestamp 2025-01-01TOO:OO:OO
    #                              --computation_class last-24h|all-of-wiki-time
    #                              --computation_dt 2025-01-01TOO:OO:OO
    #                              --mariadb_username research
    #                              --mariadb_password_file /a/b/c
    args = [ '--wiki_id', 'enwiki',
             '--target_table', 'wmf_dumps.wikitext_raw_rc2',
             '--results_table', 'wmf_dumps.wikitext_mismatch_rows_rc1',
             '--min_timestamp', '2024-01-01TOO:OO:OO',
             '--max_timestamp', '2025-01-01TOO:OO:OO',
             '--computation_class', 'last-24h',
             '--computation_dt', '2025-01-01TOO:OO:OO',
             '--mariadb_username', 'research',
             '--mariadb_password_file', '/a/b/c'
            ]
    parsed = parse_args(args)
    print(parsed)
    assert parsed.wiki_id == 'enwiki'
    assert parsed.target_table == 'wmf_dumps.wikitext_raw_rc2'
    assert parsed.results_table == 'wmf_dumps.wikitext_mismatch_rows_rc1'
    assert parsed.min_timestamp == '2024-01-01TOO:OO:OO'
    assert parsed.max_timestamp == '2025-01-01TOO:OO:OO'
    assert parsed.computation_class == 'last-24h'
    assert parsed.computation_dt == '2025-01-01TOO:OO:OO'
    assert parsed.mariadb_username == 'research'
    assert parsed.mariadb_password_file == '/a/b/c'


def test_blank_rev_timestamps_can_be_caught():
    mw_iso = "%Y%m%d%H%M%S"

    # assert we can catch bad rev_timestamps, which are 14 spaces, via ValueError
    with pytest.raises(ValueError):
        datetime.strptime('              ', mw_iso)

    # our default timestamp works
    assert datetime.strptime('20000101000000', mw_iso) == datetime(2000, 1, 1, 0, 0)
