from mediawiki_content_dump.emit_reconcile_events_to_kafka import parse_args


def test_parse_args():
    # Usage: emit_reconcile_events_to_kafka.py
    #                              --wiki_id enwiki
    #                              --inconsistent_rows_table wmf_dumps.wikitext_mismatch_rows_rc1
    #                              --wikis_table canonical_data.wikis
    #                              --namespaces_table wmf_raw.mediawiki_project_namespace_map
    #                              --namespaces_table_snapshot 2024-10
    #                              --computation_dt 2025-01-01TOO:OO:OO
    #                              --computation_class last-24h|all-of-wiki-time
    #                              --max_events_per_iteration 100000
    #                              --event_stream_topic_prefix eqiad
    #                              --event_stream_name mediawiki_content_history_reconcile
    #                              --event_schema_version 1.2.0
    #                              --kafka_bootstrap_servers kafka-jumbo1007.eqiad.wmnet:9092
    #                              --mariadb_username research
    #                              --mariadb_password_file file
    args = ['--wiki_id', 'enwiki',
            '--inconsistent_rows_table', 'wmf_dumps.wikitext_mismatch_rows_rc1',
            '--wikis_table', 'canonical_data.wikis',
            '--namespaces_table', 'wmf_raw.mediawiki_project_namespace_map',
            '--namespaces_table_snapshot', '2024-10',
            '--computation_dt', '2025-01-01TOO:OO:OO',
            '--computation_class', 'all-of-wiki-time',
            '--max_events_per_iteration', '100000',
            '--event_stream_topic_prefix', 'eqiad',
            '--event_stream_name', 'mediawiki_content_history_reconcile',
            '--event_schema_version', '1.2.0',
            '--kafka_bootstrap_servers', 'kafka-jumbo1007.eqiad.wmnet:9092',
            '--mariadb_username', 'research',
            '--mariadb_password_file', 'file',
            ]
    parsed = parse_args(args)
    print(parsed)
    assert parsed.wiki_id == 'enwiki'
    assert parsed.inconsistent_rows_table == 'wmf_dumps.wikitext_mismatch_rows_rc1'
    assert parsed.wikis_table == 'canonical_data.wikis'
    assert parsed.namespaces_table == 'wmf_raw.mediawiki_project_namespace_map'
    assert parsed.namespaces_table_snapshot == '2024-10'
    assert parsed.computation_dt == '2025-01-01TOO:OO:OO'
    assert parsed.computation_class == 'all-of-wiki-time'
    assert parsed.max_events_per_iteration == 100_000
    assert parsed.event_stream_topic_prefix == 'eqiad'
    assert parsed.event_stream_name == 'mediawiki_content_history_reconcile'
    assert parsed.event_schema_version == '1.2.0'
    assert parsed.kafka_bootstrap_servers == 'kafka-jumbo1007.eqiad.wmnet:9092'
    assert parsed.mariadb_username == 'research'
    assert parsed.mariadb_password_file == 'file'
