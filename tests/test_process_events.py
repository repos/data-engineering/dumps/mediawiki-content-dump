from mediawiki_content_dump.process_events import parse_args


def test_parse_args():
    # Test with hour:
    # Usage: process_events.py  --source_table source.table
    #                           --target_table target.table
    #                           --year 2023
    #                           --month 2
    #                           --day 15
    #                           --hour 0
    #                           --event_categories revisions page_deletes
    args0 = ['--source_table', 'source.table',
             '--target_table', 'target.table',
             '--year', '2023',
             '--month', '2',
             '--day', '15',
             '--hour', '0',
             '--event_categories', 'revisions', 'page_deletes'
             ]
    parsed = parse_args(args0)
    print(parsed)
    assert parsed.source_table == 'source.table'
    assert parsed.target_table == 'target.table'
    assert parsed.year == 2023
    assert parsed.month == 2
    assert parsed.day == 15
    assert parsed.hour == 0
    assert parsed.event_categories == ['revisions', 'page_deletes']

    # Test without hour:
    # Usage: process_events.py  --source_table source.table
    #                           --target_table target.table
    #                           --year 2023
    #                           --month 2
    #                           --day 15
    #                           --event_categories revisions page_deletes
    args1 = ['--source_table', 'source.table',
             '--target_table', 'target.table',
             '--year', '2023',
             '--month', '2',
             '--day', '15',
             '--event_categories', 'revisions', 'page_deletes'
             ]
    parsed = parse_args(args1)
    print(parsed)
    assert parsed.source_table == 'source.table'
    assert parsed.target_table == 'target.table'
    assert parsed.year == 2023
    assert parsed.month == 2
    assert parsed.day == 15
    assert parsed.hour is None
    assert parsed.event_categories == ['revisions', 'page_deletes']
