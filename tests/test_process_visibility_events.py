from mediawiki_content_dump.process_visibility_events import parse_args


def test_parse_args():
    # Test with hour:
    # Usage: process_visibility_events.py  --source_table source.table
    #                                  --target_table target.table
    #                                  --year 2023
    #                                  --month 2
    #                                  --day 15
    #                                  --hour 0
    args0 = ['--source_table', 'source.table',
             '--target_table', 'target.table',
             '--year', '2023',
             '--month', '2',
             '--day', '15',
             '--hour', '0',
             ]
    parsed = parse_args(args0)
    print(parsed)
    assert parsed.source_table == 'source.table'
    assert parsed.target_table == 'target.table'
    assert parsed.year == 2023
    assert parsed.month == 2
    assert parsed.day == 15
    assert parsed.hour == 0

    # Test without hour:
    # Usage: process_visibility_events.py  --source_table source.table
    #                                  --target_table target.table
    #                                  --year 2023
    #                                  --month 2
    #                                  --day 15
    args1 = ['--source_table', 'source.table',
             '--target_table', 'target.table',
             '--year', '2023',
             '--month', '2',
             '--day', '15',
             ]
    parsed = parse_args(args1)
    print(parsed)
    assert parsed.source_table == 'source.table'
    assert parsed.target_table == 'target.table'
    assert parsed.year == 2023
    assert parsed.month == 2
    assert parsed.day == 15
    assert parsed.hour is None
